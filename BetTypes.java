public enum BetTypes {
    NUMBER (new String[] { "" },             35),
    HIGHLOW(new String[] { "High", "Low" },  35),
    COLOR  (new String[] { "Red", "Black" }, 35),
    ODDEVEN(new String[] { "Odd", "Even" },  35);

    private String[] options;
    private int winMultiplier;


    private BetTypes(String[] options, int winMultiplier) {
        this.options = options;
        this.winMultiplier = winMultiplier;
    }


    public String[] getOptions() {
        return this.options;
    }

    public int getWinMultiplier() {
        return this.winMultiplier;
    }
}
