import java.util.Random;

public class RouletteWheel {
    private Random rand;
    int lastSpin;

    public RouletteWheel() {
        this.rand = new Random();
        this.lastSpin = 0;
    }

    public void spin() {
        this.lastSpin = rand.nextInt(37);
    }

    public int getValue() {
        return this.lastSpin;
    }
}
