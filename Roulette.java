class Roulette {
    public static void main(String[] args) {
        RouletteWheel rouletteWheel = new RouletteWheel();
        int moneyWon = 0;
        int moneyWasted = 0;
        boolean continueGame = true;

        System.out.println("+------------------------+");
        System.out.println("| Welcome to Waste.money |");
        System.out.println("+------------------------+");
        System.out.println();

        while (continueGame) {
            System.out.println("Would you like to bet?");
            continueGame = InputHandler.promptBool();
            if (!continueGame) {
                break;
            }

            System.out.println("How much you would like to bet?");
            int betMoney = InputHandler.promptNumber(1, 1001);

            System.out.println("What number you would like to bet on?");
            int betNumber = InputHandler.promptNumber(37);

            rouletteWheel.spin();
            int rolled = rouletteWheel.getValue();
            System.out.println("Rolling...");
            System.out.println("The number is " + rolled);

            if (rolled == betNumber) {
                int prise = betMoney * 35;
                System.out.println("You won " + prise + "$");
                moneyWon += prise;
            }
            else {
                System.out.println("You lost " + betMoney + "$");
                moneyWasted += betMoney;
            }

            System.out.println("=====");
        }


        System.out.println("+------------------------+");
        System.out.println("|   Thanks for playing   |");
        System.out.println("+------------------------+");
        System.out.println("You wasted " + moneyWasted + "$.");
        System.out.println("You won    " + moneyWon + "$.");
        System.out.println("Your total " + (moneyWon - moneyWasted) + "$.");
    }
}
