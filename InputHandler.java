import java.util.Scanner;

public class InputHandler {
    private static final Scanner scanner;
    
    static {
        scanner = new Scanner(System.in);
    }

    public static boolean promptBool() {
        System.out.println("YES or NO:");

        while(true) {
            String result = scanner.next().toLowerCase();
    
            switch (result) {
                case "yes":
                case "y":
                    System.out.println("---");
                    return true;
    
                case "no":
                case "n":
                    System.out.println("---");
                    return false;
                
                default:
                    System.out.println("Wrong input. Please answer YES or NO:");
                    break;
            }
        }
    }

    public static int promptNumber(int minBound, int maxBound) {
        System.out.println("Enter a number between: " + minBound + " and " + (maxBound - 1));

        while(true) {
            String result = scanner.next().toLowerCase();

            try {
                int resultNumber = Integer.parseInt(result);

                if (resultNumber < minBound) {
                    System.out.println("Value entered is smaller than " + minBound + ". Please reenter:");
                }
                else if (resultNumber >= maxBound) {
                    System.out.println("Value entered is greater than " + maxBound + ". Please reenter:");
                }
                else {
                    System.out.println("---");
                    return resultNumber;
                }
            }
            catch (NumberFormatException e) {
                System.out.println("Value entered is not a number. Please reenter:");
            }
        }
    }
    public static int promptNumber(int maxBound) {
        return promptNumber(0, maxBound);
    }
    public static BetTypes promptBetType() {
        

        return BetTypes.COLOR;
    }
}
